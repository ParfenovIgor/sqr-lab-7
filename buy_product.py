import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
insert_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s,%(product)s,%(amount)s)"
get_inventory = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
add_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username = %(username)s AND product = %(product)s"

def get_connection():
    return psycopg2.connect(
        dbname="sqr-lab-7",
        user="sqr",
        password="pass",
        host="127.0.0.1",
        port=5432
    )

inventory_size = 100

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("BEGIN")

                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(get_inventory, obj)
                count = cur.fetchone()[0]
                if not count:
                    cur.execute(insert_inventory, obj)
                elif count + amount > inventory_size:
                    raise Exception("Inventory is run out of space")
                else:
                    cur.execute(add_inventory, obj)

                cur.execute("COMMIT")
            except Exception as e:
                conn.rollback()
                raise e

buy_product('Alice', 'marshmello', 1)