# Lab

## Prepare

I prepare simple initializing script `init.sql`, which prepares database.

## Part 1

I used *transactions* to remove data inconsistency. In case of one operation fails, the whole set of operations rollback.

The transaction has to be bound by *BEGIN* and *COMMIT* statements, and in case of an exception, then *rollback* funtion is called.

## Part 2

There is an additional table *Inventory*:

```
CREATE TABLE Inventory (
    username    TEXT    REFERENCES Player(username) NOT NULL,
    product     TEXT    REFERENCES Shop(product)    NOT NULL,
    amount      INT     CHECK(amount >= 0),
    UNIQUE(username, product)
);
```

Then I login as *sqr* user and show contents of tables.

Contents of tables before execution of script.

```
sqr-lab-7=# SELECT * FROM Player;
 username | balance 
----------+---------
 Alice    |     100
 Bob      |     200
(2 строки)

sqr-lab-7=# SELECT * FROM Shop;
  product   | in_stock | price 
------------+----------+-------
 marshmello |       10 |    10
(1 строка)

sqr-lab-7=# SELECT * FROM Inventory;
 username | product | amount 
----------+---------+--------
(0 строк)
```

Contents of tables after one execution of script.

```
sqr-lab-7=# SELECT * FROM Player;
 username | balance 
----------+---------
 Bob      |     200
 Alice    |      90
(2 строки)

sqr-lab-7=# SELECT * FROM Shop;
  product   | in_stock | price 
------------+----------+-------
 marshmello |        9 |    10
(1 строка)

sqr-lab-7=# SELECT * FROM Inventory;
 username |  product   | amount 
----------+------------+--------
 Alice    | marshmello |      1
(1 строка)

sqr-lab-7=#
```