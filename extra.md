# Extra

## Part 1

If after a successful transaction a function fails, then the client may try to repeat transaction. To check this case, we can add to every transaction an ID. Client after fail will do transaction with same ID. Then the server will detect, that there already was transaction with this ID.

## Part 2

* We can undo an operation in case of fail using inverse operation.
* We can use lock to avoid race conditions.
* We can apply operation only when the operation of external system was done.